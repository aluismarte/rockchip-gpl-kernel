Linux kernel for Rockchip SOCs - www.omegamoon.com
--------------
**Build instructions:**
 
- Run "make mrproper" to make sure you have no stale .o files and dependencies lying around.
- Run "./make-mk908-nand.sh" to compile the kernel for the MK908 using the prebuild toolchain.
- Run "./make-mk908-recovery.sh" to compile the kernel (Dual Boot) for the MK908 (Will work only compile into your chroot linux).
 
**Flash instructions for the MK908 device using Linux (use at own risk!):**
 
- Connect one end of the USB cable to your PC
- Press the reset button using a paperclip, and while pressed, connect the USB cable to the OTG USB port
- Release the reset button
- Run "./flash_omegamoon_kernel" to flash the kernel to the device or flash_recovery for Dual Boot
- When ready, the device will be rebooted automatically
 
**Notes:**
 
- All test of MK908 are on board V3.
- MK908 Rom of the test is [Finless 1.7](http://blog.geekbuying.com/index.php/2013/12/19/rom-download-custom-rom-for-tronsmart-mk908-fit-for-all-version/)
- Dual Boot is supose to work if you have a initramfs.
- How to of initramfs is on testing. Be carefull if you make it.
 
**Revision history:**
 
2014-02-21:
 
- Change sh helpers, add config for Dual boot and nand boot.
 
2013-09-27:
 
- Upgraded to Linux kernel version 3.0.72+
- Merged with rockchip git
- Tested with Kingnovel K-R42 and Tronsmart MK908
- Based on the 'netxeon' Linux kernel version 3.0.36+
- Added linaro 4.8.2 toolchain
- Added build scripts
- Added linux flash tools + flash script for mk908 (use at own risk!)
- Minor config changes
 
