#!/bin/bash

THREADS="2"

if [ "$1" != "" ]; then
  THREADS="$1"
fi

# Prepare kernel
ARCH=arm CROSS_COMPILE=./omegamoon/toolchain/linaro/bin/arm-linux-gnueabihf- make rk3188_mk908_defconfig
# Compile the Kernel
ARCH=arm CROSS_COMPILE=./omegamoon/toolchain/linaro/bin/arm-linux-gnueabihf- make -j$THREADS
# Make the image
./omegamoon/rkcrc -k ./arch/arm/boot/zImage ./kernel.img
