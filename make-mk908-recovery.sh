#!/bin/bash

THREADS="4"

if [ "$1" != "" ]; then
  THREADS="$1"
fi

# Prepare kernel
make rk3188_mk908_defconfig_recovery
# Compile the Kernel
make -j$THREADS
# The image do it later
#./mkbootimg --kernel arch/arm/boot/Image --ramdisk ./initramfs.cpio --pagesize 16384 --ramdiskaddr 62000000 -o recovery.img
